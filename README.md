# Talks

These are slides for various talks I've done.

* `aaa.odp`: Talk about C++ "Almost Always Auto" style
* `coderot.odp`: Talk about code rot
* `const.odp`: Talk about const placement in C++
* `linktimeplugin.odt`: Slides for talk about link-time plug-ins. Code: https://gitlab.com/wolframroesler/linktimeplugin
* `mockless.odt`: Talk at emBO++ 2022 about mockless unit tests
* `profiling.odt`: Talk about a simple C++14 profiling class
* `scopeguards`: Talk about Boost scope guards and why to use them
* `staticanalysis.odt`: Talk about "how not to introduce static analysis"
* `tdd.odp`: Talk about Test Driven Development with Boost.Test
* `ThreadPoolTitle.odp`: Title screen for the introduction to C++ threading. Contents: https://gitlab.com/wolframroesler/ThreadPool
* `VersionTitle.odp`: Title screen for C++ version number generation. Contents: https://gitlab.com/wolframroesler/version
* `vi.odp`: How get stay in vi and live
* `config.json`: Demo file for vi talk

---
*Wolfram Rösler • wolfram@roesler-ac.de • https://gitlab.com/wolframroesler • https://mastodontech.de/@wolfram_roesler • https://www.linkedin.com/in/wolframroesler/*
